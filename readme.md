#Develop simple VK client

##Minimal requirements:

###Screens:

* *User authentication (Oauth 2.0)*: the controller contains **Login with VK** button to go VK access control page.
* *Posts list*: the controller shows posts pf user's news tape with user's names, dates, avatars, content (text or/and images) and numbers of likes and re-posts. The screen appears after authentication.
* *Post details*: the controller shows full data of the post with user's names, dates, avatars, content (text or/and images) and numbers of likes and re-posts.

###Features:

* MVC architecture
* Using [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) / [KISS](https://en.wikipedia.org/wiki/KISS_principle) / [YAGNI](https://en.wikipedia.org/wiki/You_aren%27t_gonna_need_it) principles
* Using [Gradle](https://gradle.org) to work with third-party components and build
* Have to use [VK SDK](https://new.vk.com/dev/android_sdk)
* UI have to realised with [Material design](https://material.google.com)
* Pull to load more - pull down to load more posts
* Pull to refresh - pull up to refresh
* Have to be realized login and logout procedures
* Using [SQLite](https://www.sqlite.org)
* Images must be cached
* Show asynchronous loading for images loading

###VCS:

* You need to create git repo on any public service and develop project with it.
* Frequency of comments will be rated.
* Readme.md must contain a deployment and running instruction 

###Time requirements:
* You can spend less that 8 hours.

##Additional requirements

###Features:

* Phone/Tablet support
* Portrait/Landscape support
* Using [VK API](https://new.vk.com/dev/apiusage) instead [VK SDK](https://new.vk.com/dev/android_sdk)

* Few classes have to be covered by unit-tests